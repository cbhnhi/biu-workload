package Implementation;

import java.util.Scanner;


public class Part1 extends Thread {

	int miliseconds=0;
	int seconds=0;
	int minutes=0;
	static boolean cancelationToken=true;
	Scanner in;
	public Part1()
	{	
		super();		
	}
	@Override
	public void run()
	{		
		while(cancelationToken) {};//we wait for start signal
		while(!cancelationToken) 
		{
		++miliseconds;
		if(miliseconds==1000)
			{
			miliseconds =0;
			++seconds;
			if(seconds==60)
				{
					seconds = 0;
					++minutes;
				}
			}		
		try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			System.out.println("Time: "+minutes + "::"+seconds+"::"+miliseconds);
			this.interrupt();
		}
		};
		System.out.println("Time: "+minutes + "::"+seconds+"::"+miliseconds);
	}	
	@SuppressWarnings("deprecation")
	@Override
	public void interrupt()
	{
		System.out.println("Time: "+minutes + "::"+seconds+"::"+miliseconds);
		this.suspend();
	}
	
}
