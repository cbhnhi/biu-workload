package Implementation;

public class Part2 extends Thread {
		
	
	public static int result=1;
	public int index;
	int[] arrayRef;
	Part2()
	{
		this(0,null);
	}
	
	Part2(int index,int[] arrayReff)
	{
		super();
		this.index=index;
		this.arrayRef=arrayReff;
	}
	
	@Override
	public void run()
	{
		int size = arrayRef.length;
		int currMax=Part2.result;
		for(int i =index;i<size;i=i+4)
		{
			currMax=Math.max(currMax, arrayRef[i]);
		}
		//to ensure no multiple access at same time solves static problem
		try {
			Thread.sleep((int)(Math.random()*400));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		result=Math.max(result, currMax);
	}
}
