package Implementation;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class controller {

	public static void main(String[] args) {
		System.out.println("this was created by Benjamin Pevzner and Or Bovan");
		//checks part1
		Part1Checker();
		//checks part2
		Part2Checker();
		//checks part3
		Part3Checker();
		//checks part4
		Part4Checker();
		}
	public static void Part4Checker()
	{
		Scanner in = new Scanner(System.in);
		System.out.println("type in number of tries");
		int numofTries= in.nextInt();
		for(int i =0;i<numofTries;i++)
		{
			Part4 thread= new Part4();
			thread.start();
		}
		try {
			TimeUnit.SECONDS.sleep(15);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		float a = 4*((float)Part4.C/(float)numofTries);
		System.out.println(a);
	}
	public static void Part3Checker()
	{
		int[] list = new int[] {11,15,16,13,3,14,5,66,12,124,1,78,15,26,14,112,196,192,4959,124,545,221,553,456};
		Part3 thread1 = new Part3(0,list);
		Part3 thread2 = new Part3(1,list);
		Part3 thread3 = new Part3(2,list);
		Part3 thread4 = new Part3(3,list);
		thread1.start();thread2.start();thread3.start();thread4.start();
		while(thread1.isAlive()||thread2.isAlive()||thread3.isAlive()||thread4.isAlive()) {};
		System.out.println("part3 sum is " +Part3.sum);
	}
		
	public static void Part2Checker()
	{
		
		int[] list = new int[] {11,15,16,13,3,14,5,66,12,124,1,78,15,26,14,112,196,192,4959,124,545,221,553,456};
		Part2 thread1 = new Part2(0,list);
		Part2 thread2 = new Part2(1,list);
		Part2 thread3 = new Part2(2,list);
		Part2 thread4 = new Part2(3,list);
		thread1.start();thread2.start();thread3.start();thread4.start();
		while(thread1.isAlive()||thread2.isAlive()||thread3.isAlive()||thread4.isAlive()) {};

		System.out.println("Part 2 results is" +Part2.result);
	}

public static void Part1Checker()
	{
	
	Part1 thread1=new Part1();
	Part1 thread2=new Part1();
	Part1 thread3=new Part1();
	
	thread1.start();
	thread2.start();
	thread3.start();
	Part1.cancelationToken=false;
	
	System.out.println("pls input enter when runner is done, other inputs will be ignored");
	@SuppressWarnings("resource")
	Scanner in=new Scanner(System.in);
	int iteration=0;
	while(true) {
		String a = in.nextLine();
		if(a.equals(""))
		{
			if(iteration==0)
			{
			thread1.interrupt();
			++iteration;
			continue;
			}
			else if (iteration ==1)
			{
				thread2.interrupt();
				++iteration;
				continue;
			}
			else
			{
			thread3.interrupt();
			break;
		}
		}
		
	}
	}
}
